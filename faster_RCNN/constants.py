from functools import reduce
from operator import mul as multiplication

# Data
INPUT_HEIGHT = 1600
INPUT_WIDTH = 1600
BINARIZATION_THRESHOLD = 230
PROCESS_ANYWAY = False

# Batch
INFERENCE_BATCH_SIZE = 100  # max size for a batch at evaluation time
TRAINING_BATCH_SIZE = 10  # max size for a batch at training time

# General
CLASSES = ["Diario", "Fecha", "Volanta", "Título", "Copete", "Cuerpo", "Fotografía", "Epígrafe", "Destacado", "Firma",
           "Página"]
NAME_TO_INDEX = dict(zip(CLASSES, range(len(CLASSES))))
INDEX_TO_NAME = dict(enumerate(CLASSES))
IMAGE_TENSOR_SHAPE = (1, INPUT_HEIGHT, INPUT_WIDTH)

# Paths from root folder
GROUND_TRUTH_IMG_FOLDER = "preparacion_de_datos/training/images"
GROUND_TRUTH_ANNOTATIONS_FOLDER = "preparacion_de_datos/training/annotations"
INFERENCE_IMAGES_FOLDER = "images"
PREDICTION_ANNOTATIONS_FOLDER = "predictions"
MODEL_WEIGHTS_PATH = "preparacion_de_datos/model_weights.pth"
EMPTY_MODEL_PATH = "preparacion_de_datos/empty_model.pth"

# Region Proposal Network
# Anchors
SCALES = [1, 2, 4]
ASPECT_RATIOS = [0.5, 1, 2]
POSITIVE_THRESHOLD = 0.7
NEGATIVE_THRESHOLD = 0.3
# Loss
REGRESSION_WEIGHT = 5
CONFIDENCE_WEIGHT = 1
# Proposal Module
PROPOSAL_CONV_CHANNELS = 512
PROPOSAL_DROPOUT = 0.3

# Backbone
# EfficientNetB0
B0_FINAL_CHANNEL_WIDTH = 1280
B0_CHANNELS = [16, 24, 40, 80, 112, 192, 320]
B0_LAYERS = [1, 2, 2, 3, 3, 4, 1]
# EfficientNet
KERNELS = [3, 3, 5, 3, 5, 5, 3]
EXPANSIONS = [1, 6, 6, 6, 6, 6, 6]
STRIDES = [1, 2, 2, 2, 1, 2, 1]
FEATURE_MAP_RES = (7, 7)
BACKBONE_CONV_SURVIVAL_RATE = 0.8
CONV_SQUEEZE = 4
WIDTH_MULTIPLIER = 1
DEPTH_MULTIPLIER = 1

# Classifier
ROI_SIZE = (2, 2)
CLASSIFIER_DROPOUT = 0.3
CLASSIFIER_CONV_CHANNELS = 512


def fm_size():
    factor = 1 / reduce(multiplication, STRIDES, 1)
    return INPUT_HEIGHT * factor, INPUT_WIDTH * factor
