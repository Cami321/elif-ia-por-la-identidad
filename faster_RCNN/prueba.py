from modules.backbone import EfficientNet
from skimage.io import imread, imsave, imshow
from skimage.transform import resize
import torch
from constants import *


bone = EfficientNet()

img = imread(f"{GROUND_TRUTH_IMG_FOLDER}/Ambito financiero 1984-12--14 Astiz ante probable....tif")
img = resize(img, (INPUT_HEIGHT, INPUT_WIDTH, 1))
input_tensor = torch.from_numpy(img).permute(2, 0, 1)
print(img.shape)
input_tensor = torch.stack((input_tensor,)).float()
print(input_tensor.type())
feature_map = bone(input_tensor).detach().numpy()
print(feature_map)
imsave("preparacion_de_datos/feature_map.tif", feature_map[0][0])
