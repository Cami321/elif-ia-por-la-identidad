import torch
from torch import nn
from math import ceil

# All the constants come from here
from faster_RCNN.constants import *


class EfficientNet(nn.Module):

    def __init__(self, fmap_channels, width_mult, depth_mult, dropout, squeeze):
        super(EfficientNet, self).__init__()

        channels = 4 * ceil(int(32 * width_mult) / 4)  # rounded up to the closest multiple of 4 (kernel depth is 4)

        layers = [ConvBnAct(1, channels, kernel=3, stride=2, padding=1, groups=1)]  # list of all the layers [ module ]
        past_section_out_channels = channels

        # Scale channels and num_layers according to width and depth multipliers.
        scaled_num_channels = [4 * ceil(int(c * width_mult) / 4) for c in B0_CHANNELS]
        scaled_num_layers = [int(d * depth_mult) for d in B0_LAYERS]

        # Where i contains the section of the EfficientNet we're in
        for i in range(len(scaled_num_channels)):
            # Where j contains the layer of the section we're in
            for j in range(scaled_num_layers[i]):
                # First layer of a section takes as many input channels as the last section outputs
                input_size = past_section_out_channels if j == 0 else scaled_num_channels[i]
                layer_stride = STRIDES[i] if j == 0 else 1
                layer = MBConvN(input_size, scaled_num_channels[i], kernel_size=KERNELS[i], stride=layer_stride,
                                expansion_factor=EXPANSIONS[i], survival_prob=dropout, squeeze_factor=squeeze)
                layers.append(layer)
            past_section_out_channels = scaled_num_channels[i]

        layers.append(ConvBnAct(past_section_out_channels, fmap_channels, kernel=1, stride=1, padding=0))
        self.network = nn.Sequential(*layers)

    def forward(self, x):
        return self.network(x)


class MBConvN(nn.Module):

    def __init__(self, input_size, n_out, kernel_size, stride, expansion_factor, squeeze_factor, survival_prob):
        super(MBConvN, self).__init__()

        # Is allowed to get skipped by the Stochastic Depth if the input and output size are fully the same
        self.can_be_skipped = (stride == 1 and input_size == n_out)
        # Defines the thickness of the thick part of the MBConv
        intermediate_channels = int(input_size * expansion_factor)
        # With this formula you get the padding necessary for the kernel to fit at every point
        padding = (kernel_size - 1) // 2
        # The SqueezeExcitation layer will squeeze the input to one squeeze_factor~th of the input size
        reduced_dim = int(input_size // squeeze_factor)

        self.expand = nn.Identity() if (expansion_factor == 1) else ConvBnAct(input_size, intermediate_channels, kernel=1)
        self.depthwise_conv = ConvBnAct(intermediate_channels, intermediate_channels, kernel=kernel_size, stride=stride, padding=padding, groups=intermediate_channels)
        self.se = SqueezeExcitation(intermediate_channels, squeeze=reduced_dim)
        self.pointwise_conv = ConvBnAct(intermediate_channels, n_out, kernel=1, act=False)
        self.drop_layers = StochasticDepth(survival_prob=survival_prob)

    def forward(self, x):
        residual = x

        x = self.expand(x)
        x = self.depthwise_conv(x)
        x = self.se(x)
        x = self.pointwise_conv(x)

        if self.can_be_skipped:
            x = self.drop_layers(x)
            x += residual

        return x


class ConvBnAct(nn.Module):

    def __init__(self, n_in, n_out, kernel, stride=1, padding=0, groups=1, batch_norm=True, act=True, bias=False):
        super(ConvBnAct, self).__init__()

        self.conv = nn.Conv2d(n_in, n_out, kernel_size=kernel, stride=stride, padding=padding, groups=groups, bias=bias)
        self.batch_norm = nn.BatchNorm2d(n_out) if batch_norm else nn.Identity()
        self.activation = nn.SiLU() if act else nn.Identity()

    def forward(self, x):
        x = self.conv(x)
        x = self.batch_norm(x)
        x = self.activation(x)

        return x


class SqueezeExcitation(nn.Module):

    def __init__(self, input_size, squeeze):
        super(SqueezeExcitation, self).__init__()

        self.se = nn.Sequential(
            nn.AdaptiveAvgPool2d(1),
            nn.Conv2d(input_size, squeeze, kernel_size=1),
            nn.SiLU(),
            nn.Conv2d(squeeze, input_size, kernel_size=1),
            nn.Sigmoid()
        )

    def forward(self, x):
        y = self.se(x)

        return x * y


class StochasticDepth(nn.Module):

    def __init__(self, survival_prob):
        super(StochasticDepth, self).__init__()

        self.prob = survival_prob

    def forward(self, x):
        if not self.training:
            return x

        binary_tensor = torch.rand(x.shape[0], 1, 1, 1, device=x.device) < self.prob  # One roll for every batch

        return torch.div(x, self.prob) * binary_tensor  # ??? div ???
