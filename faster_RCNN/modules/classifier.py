import torch
from torch import nn
from torchvision import ops

from faster_RCNN.constants import *


class ClassificationModule(nn.Module):
    def __init__(self, fm_channels, hidden_dim, dropout, roi_size):
        super().__init__()
        self.roi_size = roi_size
        # hidden network
        self.avg_pool = nn.AvgPool2d(self.roi_size)
        self.fc = nn.Linear(fm_channels, hidden_dim)
        self.dropout = nn.Dropout(dropout)

        # define classification head
        self.cls_head = nn.Linear(hidden_dim, len(CLASSES))

    def forward(self, feature_map, proposals_list, gt_classes=None):

        if gt_classes is None:
            mode = 'eval'
        else:
            mode = 'train'

        # apply roi pooling on proposals followed by avg pooling
        roi_out = ops.roi_pool(feature_map, proposals_list, self.roi_size)
        roi_out = self.avg_pool(roi_out)

        # flatten the output
        roi_out = roi_out.squeeze(-1).squeeze(-1)

        # pass the output through the hidden network
        out = self.fc(roi_out)
        out = nn.functional.relu(self.dropout(out))

        # get the classification scores
        cls_scores = self.cls_head(out)

        if mode == 'eval':
            return cls_scores

        # compute cross entropy loss
        cls_loss = nn.functional.cross_entropy(cls_scores, gt_classes.long())

        return cls_loss
