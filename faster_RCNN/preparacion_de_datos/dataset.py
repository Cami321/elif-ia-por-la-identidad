import torch
import numpy as np
from torch.utils.data import Dataset
from torch.nn.utils.rnn import pad_sequence
from os.path import exists as path_exists
from os import listdir
from json import load as json_load
from skimage.io import imread, imsave
from skimage.transform import resize as img_resize
from torchvision.ops import box_convert, box_iou
from torchvision.transforms.functional import resize, crop, pad
from torch import Tensor
from math import ceil, floor

# All the constants come from here
from faster_RCNN.constants import *


def get_training_data():
    # gets the names (and only the names) of every image at the SOURCE_IMAGES_FOLDER folder
    img_names = [f.rsplit(".", 1)[0] for f in listdir(f"{GROUND_TRUTH_IMG_FOLDER}/")]

    for name in img_names:
        if not path_exists(f"{GROUND_TRUTH_ANNOTATIONS_FOLDER}/{name}.json"):
            print(f"WARNING: Could not find annotations for image {name}.tif")
            img_names.remove(name)
            continue

    return TrainingDataset(img_names)


class TrainingDataset(Dataset):
    def __init__(self, img_names):
        self.img_names = img_names
        self.img_data_all, self.gt_bboxes_all, self.gt_classes_all = self._get_data()

    def __len__(self):
        return self.img_data_all.size(dim=0)

    def __getitem__(self, idx):
        return self.img_data_all[idx], self.gt_bboxes_all[idx], self.gt_classes_all[idx]

    def _get_data(self):
        img_tensors = []
        gt_idxs_all = []
        all_boxes = []

        gt_boxes, gt_classes = get_all_annotations(self.img_names)

        for i, img_name in enumerate(self.img_names):
            path = f"{GROUND_TRUTH_IMG_FOLDER}/{img_name}.tif"
            # skip if the image path is not valid
            if (not img_name) or (not path_exists(path)):
                continue

            # read and preprocess image
            img = imread(path)
            img_tensor = preprocessing(img)
            images, bboxes, classes = img_augmentation(img_tensor, gt_boxes[i], gt_classes[i])

            all_boxes += bboxes
            img_tensors += images

            # encode class names as integers
            for img_classes in classes:
                gt_idx = torch.Tensor([NAME_TO_INDEX[name] for name in img_classes])
                gt_idxs_all.append(gt_idx)

        # pad bounding boxes and classes, so they are of the same size
        gt_bboxes_pad = pad_sequence(gt_boxes, batch_first=True, padding_value=-1)
        gt_classes_pad = pad_sequence(gt_idxs_all, batch_first=True, padding_value=-1)

        # stacks img_tensors into a single tensor creating the image dimension in the process
        # Tensor of shape (images = len(self.img_names), channels = 1, height = INPUT_HEIGHT, width = INPUT_WIDTH)
        img_data_stacked = torch.stack(img_tensors, dim=0)

        return img_data_stacked.to(dtype=torch.float), gt_bboxes_pad, gt_classes_pad


class InferenceDataset(Dataset):
    def __init__(self, img_fnames):
        self.img_fnames = img_fnames
        self.data = self._get_data()

    def _get_data(self):
        img_tensors = []  # list of the tensor representation of every image read
        for filename in self.img_fnames:
            img = imread(
                f"{INFERENCE_IMAGES_FOLDER}/{filename}")  # reads images from the folder at INFERENCE_IMAGE_FOLDER
            tensor = preprocessing(img)
            img_tensors.append(tensor)

        # stacks img_tensors into a single tensor creating the image dimension in the process
        # Tensor of shape (images = len(self.img_names), channels = 1, height = INPUT_HEIGHT, width = INPUT_WIDTH)
        batch_tensor = torch.stack(img_tensors, dim=0).to(dtype=torch.float)  # tensor representation of the whole batch
        return batch_tensor

    class BatchProvider:
        def __init__(self, data, batch_size: int):
            self.provision = data.data
            self.b_size = batch_size
            self.provided = 0

        def __call__(self):
            end_of_batch = self.provided + self.b_size
            end_index = end_of_batch if end_of_batch < self.provision.shape[0] else self.provision.shape[0]
            end_of_provision = end_index == self.provision.shape[0]
            output = self.provision[self.provided: end_index], end_of_provision
            self.provided = end_index
            return output


def preprocessing(img):
    #  converts to a tensor of shape:
    #  channels = 1 | height = img height | width = img width
    tensor: Tensor = torch.from_numpy(img)[:, :, 0]
    tensor = torch.reshape(tensor, (1, img.shape[0], img.shape[1]))

    # binarization step
    # every element (pixel) in the tensor gets compared with a threshold
    # if it's lower: that pixel corresponds with a black pixel (value 0) on binary_tensor
    # otherwise: it corresponds with a white pixel (value 1 on binary_tensor, 255 in tensor after multiplication)
    binary_tensor: Tensor = (tensor >= BINARIZATION_THRESHOLD).to(dtype=torch.float)
    tensor = binary_tensor

    return tensor


# Augmentates a single image
def img_augmentation(image: Tensor, boxes: [[float]], classes: [int], threshold_w=(1 / 6), threshold_h=(1 / 6),
                     threshold_iou=0, max_padding=15):
    all_imgs = [image]
    all_classes = [classes]
    all_bboxes = [boxes]
    rand = torch.rand(10, 4).tolist()
    for x, y, w, h in rand:
        # Coordinates of the crop
        thresh_h = threshold_h * image.shape[1]
        thresh_w = threshold_w * image.shape[2]
        x = x * image.shape[2]
        y = y * image.shape[1]
        w = floor(w * (image.shape[2] - thresh_w) + thresh_w)
        h = floor(h * (image.shape[1] - thresh_h) + thresh_h)
        x = floor(x) if x + w < image.shape[2] else image.shape[2] - w
        y = floor(y) if y + h < image.shape[1] else image.shape[1] - h

        # Crop
        img_crop = crop(image.clone(), y, x, h, w)
        all_imgs.append(img_crop)

        frame = torch.from_numpy(np.array([int(x), int(y), int(x + w), int(y + h)]))
        boxes_crop = [box_convert(torch.from_numpy(np.array(box)), "cxcywh", "xyxy") for box in boxes]
        ground_truth = [(b, c) for b, c in zip(boxes_crop, classes) if box_iou(torch.reshape(b, [1]+list(b.shape)), torch.reshape(frame, [1]+list(frame.shape))) > threshold_iou]
        all_classes.append([c for _, c in ground_truth])
        all_bboxes.append(torch.from_numpy(np.array([_clip_inside(frame, b) for b, _ in ground_truth])))

    pad_imgs = []
    pad_bboxes = []
    pad_classes = []
    rand = (torch.rand(5, 4) * max_padding).tolist()
    for top, bottom, left, right in rand:
        top = floor(top)
        bottom = floor(bottom)
        left = floor(left)
        right = floor(right)
        for img, org_box, clas in zip(all_imgs, all_bboxes, all_classes):
            # One side
            pad_imgs.append(pad(img, [left, 0, 0, 0]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, left=left)
            pad_imgs.append(pad(img, [0, top, 0, 0]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, top=top)
            pad_imgs.append(pad(img, [0, 0, right, 0]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas)
            pad_imgs.append(pad(img, [0, 0, 0, bottom]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas)

            # Two sides
            pad_imgs.append(pad(img, [left, top, 0, 0]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, left=left, top=top)
            pad_imgs.append(pad(img, [left, 0, right, 0]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, left=left)
            pad_imgs.append(pad(img, [left, 0, 0, bottom]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, left=left)
            pad_imgs.append(pad(img, [0, top, right, 0]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, top=top)
            pad_imgs.append(pad(img, [0, top, 0, bottom]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, top=top)
            pad_imgs.append(pad(img, [0, 0, right, bottom]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas)

            # Three sides
            pad_imgs.append(pad(img, [left, top, right, 0]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, left=left, top=top)
            pad_imgs.append(pad(img, [left, top, 0, bottom]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, left=left, top=top)
            pad_imgs.append(pad(img, [left, 0, right, bottom]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, left=left)
            pad_imgs.append(pad(img, [0, top, right, bottom]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, top=top)

            # Four sides
            pad_imgs.append(pad(img, [left, top, right, bottom]))
            _append_modified(pad_bboxes, pad_classes, org_box, clas, left=left, top=top)
    all_imgs += pad_imgs
    all_bboxes += pad_bboxes
    all_classes += pad_classes

    new_all_img = []
    new_all_bboxes = []
    for img, bboxes in zip(all_imgs, all_bboxes):
        h, w = img.shape[1:]
        img = resize(img, [INPUT_HEIGHT, INPUT_WIDTH])
        for bbox in bboxes:
            print(bbox)
            b_x, b_y, b_w, b_h = bbox
            bbox = [_scale(INPUT_WIDTH, w, b_x), _scale(INPUT_HEIGHT, h, b_y), _scale(INPUT_WIDTH, w, b_w), _scale(INPUT_HEIGHT, h, b_h)]
            new_all_bboxes.append(bbox)
        new_all_img.append(img)


    return new_all_img, new_all_bboxes, all_classes


def get_all_annotations(img_names):
    boxes_list = []  # [ [ [ 4 floats ] <- bounding box ] <- image ]
    classes_list = []  # [ [ class names ] <- image ]
    for img_name in img_names:
        img_path = f"{GROUND_TRUTH_IMG_FOLDER}/{img_name}.tif"
        img = imread(img_path)
        size = img.shape[:2]
        img_boxes, img_classes = get_annotation(img_name, size)
        boxes_list.append(torch.Tensor(img_boxes))
        classes_list.append(img_classes)
    return boxes_list, classes_list


def get_annotation(img_name, size):
    boxes = []  # [ [ 4 Floats ] <- bounding box ]
    classes = []  # [ label names ]

    path = f"{GROUND_TRUTH_ANNOTATIONS_FOLDER}/{img_name}.json"
    with open(path, "r") as js:
        js_dict = json_load(js)

        if js_dict["Diario"]["bounding_box"]:
            add_bounding_box(js_dict["Diario"]["bounding_box"], size, "Diario", boxes, classes)

        if js_dict["Fecha"]["bounding_box"]:
            add_bounding_box(js_dict["Fecha"]["bounding_box"], size, "Página", boxes, classes)

        for bounding_box in get_bounding_boxes(js_dict["Página"]):
            add_bounding_box(bounding_box, size, "Página", boxes, classes)

        for note in js_dict["Notas"]:
            for label_class in note.keys():
                for bounding_box in get_bounding_boxes(note[label_class]):
                    add_bounding_box(bounding_box, size, label_class, boxes, classes)

        if len(boxes) != len(classes):
            print("ERROR: theres not the same amount of boxes that of classes!!!")
            error = 1 / 0
            print(error)

    return boxes, classes


def add_bounding_box(b_box, size, label_class, boxes, classes):
    img_h, img_w = size
    x, y, h, w = scale_bounding_box(b_box["x"], b_box["y"], b_box["height"], b_box["width"], img_h, img_w)
    boxes.append([x, y, x + w, y + h])
    classes.append(label_class)


def scale_bounding_box(x, y, height, width, img_height, img_width):
    scale_w = INPUT_WIDTH / img_width
    scale_h = INPUT_HEIGHT / img_height

    scaled_w = round(width * scale_w)
    scaled_h = round(height * scale_h)
    scaled_x = round(x * scale_w)
    scaled_y = round(y * scale_h)

    return scaled_x, scaled_y, scaled_h, scaled_w


def get_bounding_boxes(labels: [dict]):
    return [label["bounding_box"] for label in labels]


def _clip_inside(frame: torch.Tensor, box: torch.Tensor):
    # frame & box -> [x1, y1, x2, y2]
    fx1, fy1, fx2, fy2 = frame.tolist()
    x1, y1, x2, y2 = box.tolist()
    x1 = x1 - fx1 if x1 > fx1 else 0
    y1 = y1 - fy1 if y1 > fy1 else 0
    x2 = x2 - fx1 if x2 < fx2 else fx2
    y2 = y2 - fy1 if y2 < fy2 else fy2

    tensor = torch.from_numpy(np.array([int(x1), int(y1), int(x2), int(y2)]))
    tensor = box_convert(tensor, "xyxy", "cxcywh")
    return tensor.tolist()


def _append_modified(box_list, class_list, boxes, classes, left=0, top=0):
    for box, clas in zip(boxes, classes):
        x, y, w, h = box
        box_list.append(torch.from_numpy(np.array([x + left, y + top, w, h])))
        class_list.append(clas)


def _scale(new_len, old_len, prop):
    return new_len * prop / old_len
