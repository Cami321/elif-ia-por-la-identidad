import torch
from torch import nn
from modules.backbone import EfficientNet
from modules.region_proposal import RegionProposalNetwork
from modules.classifier import ClassificationModule
from math import ceil
from faster_RCNN.constants import *


class FasterRCNN(nn.Module):
    def __init__(
            # Backbone Parameters
            self, width_mult=WIDTH_MULTIPLIER, depth_mult=DEPTH_MULTIPLIER, b_dropout=BACKBONE_CONV_SURVIVAL_RATE, squeeze=CONV_SQUEEZE,
            # Region Proposal Parameters
            input_size=(INPUT_HEIGHT, INPUT_WIDTH), aspect_ratios=ASPECT_RATIOS, scales=SCALES,
            p_thresh=POSITIVE_THRESHOLD,
            n_thresh=NEGATIVE_THRESHOLD, conf_w=CONFIDENCE_WEIGHT, reg_w=REGRESSION_WEIGHT,
            proposal_hidden=PROPOSAL_CONV_CHANNELS,
            rpn_dropout=PROPOSAL_DROPOUT,
            # Classifier Parameters
            hidden_dim=CLASSIFIER_CONV_CHANNELS, c_dropout=CLASSIFIER_DROPOUT, roi_size=ROI_SIZE):

        super().__init__()
        fmap_channels = ceil(B0_FINAL_CHANNEL_WIDTH * width_mult)
        self.feature_extractor = EfficientNet(fmap_channels, width_mult, depth_mult, b_dropout, squeeze)
        self.rpn = RegionProposalNetwork(fmap_channels, input_size, fm_size(), aspect_ratios, scales, p_thresh,
                                         n_thresh, conf_w, reg_w, proposal_hidden, rpn_dropout)
        self.classifier = ClassificationModule(fmap_channels, hidden_dim, c_dropout, roi_size)
        self.apply(init_parameters)

    def forward(self, images, gt_bboxes, gt_classes):
        feature_map = self.feature_extractor(images)

        total_rpn_loss, proposals, positive_anc_ind_sep, gt_class_pos = self.rpn(images, gt_bboxes, gt_classes,
                                                                                 feature_map)

        # get separate proposals for each sample
        pos_proposals_list = []
        batch_size = images.size(dim=0)
        for idx in range(batch_size):
            proposal_idxs = torch.where(positive_anc_ind_sep == idx)[0]
            proposals_sep = proposals[proposal_idxs].detach().clone()
            pos_proposals_list.append(proposals_sep)

        cls_loss = self.classifier(feature_map, pos_proposals_list, gt_class_pos)
        total_loss = cls_loss + total_rpn_loss

        return total_loss

    def inference(self, images, conf_thresh=0.5, nms_thresh=0.7):
        feature_map = self.feature_extractor(images)

        batch_size = images.size(dim=0)
        proposals_final, conf_scores_final = self.rpn.inference(images, feature_map, conf_thresh, nms_thresh)
        cls_scores = self.classifier(feature_map, proposals_final)

        # convert scores into probability
        cls_probs = nn.functional.softmax(cls_scores, dim=-1)
        # get classes with highest probability
        classes_all = torch.argmax(cls_probs, dim=-1)

        classes_final = []
        # slice classes to map to their corresponding image
        c = 0
        for i in range(batch_size):
            n_proposals = len(proposals_final[i])  # get the number of proposals for each image
            classes_final.append(classes_all[c: c + n_proposals])
            c += n_proposals

        return proposals_final, conf_scores_final, classes_final


def init_parameters(module):
    if isinstance(module, (nn.Conv2d, nn.BatchNorm2d, nn.Linear)):
        if module.weight is not None:
            module.weight.data.normal_()
        if module.bias is not None:
            module.bias.data.normal_()
