import torch
from torch.utils.data import DataLoader
from preparacion_de_datos.dataset import get_training_data, InferenceDataset
from model import FasterRCNN
from tqdm import tqdm
from os import listdir
from faster_RCNN.constants import *
import json

from skimage.io import imread
from skimage.transform import resize
import skimage


def train(model, learning_rate, dataloader, epochs):
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    model.train()
    loss_list = []

    for i in tqdm(range(epochs)):
        total_loss = 0
        for batch, gt_bboxes, gt_classes in dataloader:
            loss = model(batch, gt_bboxes, gt_classes)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            total_loss += loss.item()
        loss_list.append(total_loss)

    torch.save(model.state_dict(), MODEL_WEIGHTS_PATH)
    return loss_list


def run_inference():
    # filters the files that have the wrong extension (under the assumption that they aren't .tif/.tiff files)
    img_filenames = [f for f in listdir(f"{INFERENCE_IMAGES_FOLDER}/")]
    for filename in img_filenames:
        extension = filename.rsplit(".", 1)[1]
        if extension != "tif" and extension != "tiff":
            print(f"WARNING/INFO: found file {filename} with an unexpected extension: .{extension}")
            img_filenames.remove(filename)
            continue

    # loads the model
    dataset = InferenceDataset(img_filenames)
    provider = InferenceDataset.BatchProvider(dataset, INFERENCE_BATCH_SIZE)
    model = FasterRCNN()
    model.load_state_dict(torch.load(MODEL_WEIGHTS_PATH))
    model.eval()

    '''
    creates json files with the following structure 
    dict  |
        class names : [dict]  |
            {"confidence": float, "bbox": [float, float, float, float]}
    '''
    # evaluates the image for each image and stores the results as json
    end_of_input = False
    image_ix = 0
    while not end_of_input:
        batch, end_of_input = provider()
        label_dict = dict([(clas, []) for clas in CLASSES])

        # returns a tuple of lists of lists where same first list index means they are from the same image
        batch_bbox, batch_conf, batch_clas = model.inference(batch)

        # transforms into a list of tuples of lists where each tuple represents an image's output
        list_tuple_list = zip(batch_bbox, batch_conf, batch_clas)

        for img_bbox, img_conf, img_clas in list_tuple_list:
            for bbox, conf, clas in zip(img_bbox, img_conf, img_clas):
                clas = INDEX_TO_NAME[clas.item()]
                box_dict = {"confidence": conf.item(), "bbox": bbox.tolist()}
                label_dict[clas].append(box_dict)
            json_str = json.dumps(label_dict, sort_keys=False, indent=3, ensure_ascii=False)
            with open(f"{PREDICTION_ANNOTATIONS_FOLDER}/{img_filenames[image_ix]}.json", "w+") as file:
                file.write(json_str)
            image_ix += 1


def test():
    data = get_training_data()
    dataloader = DataLoader(data, TRAINING_BATCH_SIZE, shuffle=True)
    model = FasterRCNN()

    losses = train(model, 0.01, dataloader, 5)
    print(losses)

    run_inference()


test()
