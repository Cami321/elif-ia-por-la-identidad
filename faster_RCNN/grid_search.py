from torch.utils.data import DataLoader, random_split
from model import FasterRCNN
from preparacion_de_datos.dataset import get_training_data
from constants import *
import numpy as np
import torch
from tqdm import tqdm
import pickle


def grid_train(model, K=3, learning_rate=0.1, epochs=100):
    batches = random_split(get_training_data(), [1/K for _ in range(K)])
    print("was here")
    torch.save(model.state_dict(), EMPTY_MODEL_PATH)
    cross_validation = [0 for _ in range(K)]

    for i, subset in enumerate(batches):
        model.load_state_dict(torch.load(EMPTY_MODEL_PATH))
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
        data = batches.copy()
        data.remove(subset)
        dataloader = DataLoader(sum([s_set for j, s_set in enumerate(batches) if i != j]), batch_size=TRAINING_BATCH_SIZE, shuffle=True)

        for _ in tqdm(range(epochs)):
            for batch, gt_bboxes, gt_classes in dataloader:
                loss = model(batch, gt_bboxes, gt_classes)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                cross_validation[i] = loss.item()

    return sum(cross_validation) / K




PARAMETER_NAMES = ["EFFICIENTNET_MULTIPLIERS", "ROI_SIZE", "BACKBONE_CONV_SURVIVAL_RATE", "CONV_SQUEEZE", "INPUT_HEIGHT",
                   "INPUT_WIDTH", "ASPECT_RATIOS", "SCALES", "POSITIVE_THRESHOLD", "NEGATIVE_THRESHOLD",
                   "CONFIDENCE_WEIGHT", "REGRESSION_WEIGHT", "PROPOSAL_CONV_CHANNELS", "PROPOSAL_DROPOUT",
                   "CLASSIFIER_CONV_CHANNELS", "CLASSIFIER_DROPOUT"]

PARAMETER_DEFAULT = [[WIDTH_MULTIPLIER, DEPTH_MULTIPLIER], ROI_SIZE, BACKBONE_CONV_SURVIVAL_RATE, CONV_SQUEEZE, INPUT_HEIGHT,
                     INPUT_WIDTH, ASPECT_RATIOS, SCALES, POSITIVE_THRESHOLD, NEGATIVE_THRESHOLD, CONFIDENCE_WEIGHT,
                     REGRESSION_WEIGHT, PROPOSAL_CONV_CHANNELS, PROPOSAL_DROPOUT, CLASSIFIER_CONV_CHANNELS,
                     CLASSIFIER_DROPOUT]


def get_model(hyperparams: dict):
    efficientnet_multipliers, roi_size, backbone_conv_survival_rate, conv_squeeze, input_height, input_width, \
        aspect_ratios, scales, positive_threshold, negative_threshold, confidence_weight, regression_weight, \
        proposal_conv_channels, proposal_dropout, classifier_conv_channels, classifier_dropout = PARAMETER_DEFAULT

    for key, value in hyperparams.items():
        match PARAMETER_NAMES.index(key):
            case 0:
                efficientnet_multipliers = value
            case 1:
                roi_size = value
            case 2:
                backbone_conv_survival_rate = value
            case 3:
                conv_squeeze = value
            case 4:
                input_height = value
            case 5:
                input_width = value
            case 6:
                aspect_ratios = value
            case 7:
                scales = value
            case 8:
                positive_threshold = value
            case 9:
                negative_threshold = value
            case 10:
                confidence_weight = value
            case 11:
                regression_weight = value
            case 12:
                proposal_conv_channels = value
            case 13:
                proposal_dropout = value
            case 14:
                classifier_conv_channels = value
            case 15:
                classifier_dropout = value
            case _:
                print("WARNING: received an unexpected argument keyword at grid_search.get_model(*kwargs)")

    return FasterRCNN(efficientnet_multipliers[0], efficientnet_multipliers[1], backbone_conv_survival_rate,
                      conv_squeeze,
                      (input_height, input_width), aspect_ratios, scales, positive_threshold, negative_threshold,
                      confidence_weight, regression_weight, proposal_conv_channels, proposal_dropout,
                      classifier_conv_channels, classifier_dropout, roi_size)


class HyperparameterHandler:
    def __init__(self, kwargs: dict):
        self.params = []
        self.active = []
        dimensions = []

        for key, value in kwargs.items():
            self.active.append(key)
            self.params.append(HyperparameterContainer(key, value))
            dimensions.append(len(value))

        self.n_act = len(self.active)
        self.config = [self.params[i]() for i in range(self.n_act)]
        self.working = True
        self.tensor = torch.zeros(dimensions)

    def __call__(self, *args, **kwargs):
        ret_conf = self.config.copy()

        # new config
        move_idx = self.n_act - 1
        while not self.params[move_idx].can_call():
            self.params[move_idx].reset()
            self.config[move_idx] = self.params[move_idx]()
            move_idx -= 1
            if move_idx == -1:
                self.working = False
                return dict(zip(self.active, ret_conf))
        self.config[move_idx] = self.params[move_idx]()

        # returns old config
        return dict(zip(self.active, ret_conf))

    def still_working(self):
        return self.working

    def get_tensor(self):
        return self.tensor.clone()


class HyperparameterContainer:
    def __init__(self, name, params):
        self.name = name
        self.params = params
        self.i = 0

    def __call__(self):
        self.i += 1
        return self.params[self.i - 1]

    def can_call(self):
        return self.i < len(self.params)

    def parameter_name(self):
        return self.name

    def reset(self):
        if not self.can_call():
            self.i = 0
        else:
            print("WARNING: Tried to reset a HyperparameterContainer but it wasn't fully used yet")


def grid_search(**kwargs):
    handler = HyperparameterHandler(kwargs)
    tensor = handler.get_tensor()
    tensor_shape = tensor.shape
    evaluations = torch.reshape(tensor, (-1,)).tolist()
    idx = 0

    while handler.still_working():
        state = handler()
        model = get_model(state)
        evaluations[idx] = grid_train(model)

    tensor = torch.reshape(torch.from_numpy(np.array(evaluations)), tensor_shape).tolist()

    with open("grid_found.pkl", "wb") as f:
        pickle.dump(tensor, f)


grid_search(BACKBONE_CONV_SURVIVAL_RATE=[0.6, 0.7, 0.8, 0.9], CONV_SQUEEZE=[2, 3, 4, 5, 6], POSITIVE_THRESHOLD=[0.6, 0.65, 0.7, 0.75, 0.8],
            NEGATIVE_THRESHOLD=[0.2, 0.25, 0.3, 0.35, 0.4], CONFIDENCE_WEIGHT=[0.5, 1, 1.5, 2], REGRESSION_WEIGHT=[4, 4.5, 5, 5.5], PROPOSAL_CONV_CHANNELS=[424, 512, 664],
            PROPOSAL_DROPOUT=[0.2, 0.25, 0.3, 0.4], CLASSIFIER_CONV_CHANNELS=[424, 512, 664], CLASSIFIER_DROPOUT=[0.2, 0.25, 0.3, 0.4], ROI_SIZE=[(2, 2), (3, 3)])
