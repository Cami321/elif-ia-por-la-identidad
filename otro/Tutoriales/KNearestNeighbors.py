import sklearn
from sklearn.utils import shuffle
from sklearn.neighbors import KNeighborsClassifier
from sklearn import linear_model, preprocessing
import pandas as pd
import numpy as np
from matplotlib import pyplot
from matplotlib import style


data = pd.read_csv("data/car.data")

le = preprocessing.LabelEncoder()
buying = le.fit_transform(list(data["buying"]))
maint = le.fit_transform(list(data["maint"]))
door = le.fit_transform(list(data["door"]))
persons = le.fit_transform(list(data["persons"]))
lug_boot = le.fit_transform(list(data["lug_boot"]))
safety = le.fit_transform(list(data["safety"]))
cls = le.fit_transform(list(data["class"]))

precision = 5
max_neighbors = 100

X = list(zip(buying, door, maint, persons, lug_boot, safety))
Y = list(cls)
"""results_data = list()
for j in range(precision):
    partial = list()
    for i in range(6, max_neighbors):
        x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, Y, test_size=0.1)
        model = KNeighborsClassifier(n_neighbors=i)
        model.fit(x_train, y_train)
        partial.append(model.score(x_test, y_test))
    results_data.append(partial)

results = list()
for i in range(max_neighbors-6):
    mean = 0
    for j in range(1, precision):
        mean += results_data[j][i]
    mean /= precision
    results.append(mean)

style.use("ggplot")
pyplot.scatter(range(6, max_neighbors), results)
pyplot.xlabel("Neighbors Searched")
pyplot.ylabel("Mean Accuracy")
pyplot.show()"""

x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, Y, test_size=0.1)
model = KNeighborsClassifier(n_neighbors=7)
model.fit(x_train, y_train)
print(model.score(x_test, y_test))

