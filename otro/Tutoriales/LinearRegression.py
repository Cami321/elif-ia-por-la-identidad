import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn import model_selection
import pickle

MODEL_FILE_DIR = "models/grade_prediction.pickle"

data = pd.read_csv("data/student-mat.csv", sep=";")
print(data.head())

data = data[["G1", "G2", "G3", "studytime", "failures", "absences"]]
print(data.head())

predict_label = "G3"

X = np.array(data.drop([predict_label], axis=1))
Y = np.array(data[predict_label])

x_train, x_test, y_train, y_test = model_selection.train_test_split(X, Y, test_size=0.1)

best = 0
for _ in range(100):
    x_train, x_test, y_train, y_test = model_selection.train_test_split(X, Y, test_size=0.1)
    model = linear_model.LinearRegression()
    model.fit(x_train, y_train)
    acc = model.score(x_test, y_test)
    print(acc)
    if acc > best:
        best = acc
        with open(MODEL_FILE_DIR, "wb") as f:
            pickle.dump(model, f)

pickle_read = open(MODEL_FILE_DIR, "rb")
model = pickle.load(pickle_read)
pickle_read.close()

print(model.score(x_test, y_test))

print("Ecuacion de la linea:", model.coef_, "x +", model.intercept_)