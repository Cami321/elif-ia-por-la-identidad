import sklearn
from sklearn.model_selection import train_test_split
from sklearn import datasets
from sklearn import svm

full_data = datasets.load_breast_cancer()

x_train, x_test, y_train, y_test = train_test_split(full_data["data"], full_data["target"], test_size=0.05)
