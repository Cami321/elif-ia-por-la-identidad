import tensorflow as tf
#from tensorflow import keras as k
from matplotlib import pyplot as plt
from os import walk
from PIL import Image
import math

files = []
for (dirpath, dirnames, filenames) in walk("images"):
    files += filenames

widths = []
heights = []
for f_name in files:
    with Image.open(f"images/{f_name}") as img:
        w, h = img.size
        widths.append(w)
        heights.append(h)
        dist_p1 = math.sqrt((w-1500)**2 + (h-1500)**2)
        dist_p2 = math.sqrt((w-5000)**2 + (h-5000)**2)
        if dist_p1 < dist_p2:
            new_size = 1500
        else:
            new_size = 5000
        image = tf.image.resize(tf.image.convert_image_dtype(img, tf.float32), (new_size, new_size))

fig, ax = plt.subplots()
ax.scatter(widths, heights)
ax.scatter([1700, 5000], [1700, 5000])
plt.xlabel("width")
plt.ylabel("height")
plt.show()

