import xml.etree.ElementTree as Tree
import json

ERROR_MARGIN = 50
MAX_STRIP = 7
MIN_EVAL = 0
POS_WEIGHT = 1
POS_BIAS = 0
STR_WEIGHT = 1
STR_BIAS = 0


def cost_function(XX, YY):
	corrects = 0
	for X, Y in zip(XX, YY):
		if full_eval(X) == Y:
			corrects += 1
	return corrects / len(XX)


class StringOut:
	def __init__(self, string, x, y, width, height, obj_id = -1):
		self.id = obj_id
		self.string = string
		self.x = x
		self.y = y
		self.w = width
		self.h = height

	def __eq__(self, other):
		if not isinstance(other, self.__class__):
			return False
		else:
			return self.string == other.string and self.x == other.x and self.y == other.y and self.w == other.w and self.h == other.h


def from_dict(dictionary):
	return StringOut(
		dictionary["CONTENT"],
		int(dictionary["VPOS"]),
		int(dictionary["HPOS"]),
		int(dictionary["WIDTH"]),
		int(dictionary["HEIGHT"]),
		int(dictionary["ID"].lstrip("string_")))


def loop_through(master):
	if master.tag.lstrip("{http://www.loc.gov/standards/alto/ns-v3#}") == "TextLine":
		return [from_dict(entry.attrib) for i, entry in enumerate(master) if i % 2 == 0]
	else:
		ret = []
		for entry in master:
			ret += loop_through(entry)
		return ret


def from_json_dict(j_dict):
	j_list = [j_dict["Diario"], j_dict["Fecha"]]
	aux = j_dict["Notas"]
	for note in aux:
		for label_type in note.keys():
			for entry in note[label_type]:
				j_list.append(entry)

	for entry in j_dict["Página"]:
		j_list.append(entry)

	return j_list


def _inside(area, string):
	return string.x >= area.x and string.y >= area.y and string.x + string.w <= area.x + area.w and string.y + string.h <= area.y + area.h


def _search(area):
	return [s for s in positioned_strings if _inside(s, area)]


def lstripi(string: str, n: int):
	ret_string = ""
	for i in range(n, len(string)):
		ret_string += string.__getitem__(i)
	return ret_string


def rstripi(string: str, n: int):
	ret_string = ""
	for i in range(0, len(string)-n):
		ret_string += string.__getitem__(i)
	return ret_string


def _eval_position(string1: StringOut, string2: StringOut):
	if _inside(string1, string2):
		return 0
	if string1.x < string2.x < (string1.x + string1.w):
		return (string1.y - string2.y)**2
	if string1.y < string2.y < (string1.y + string1.h):
		return (string1.x - string2.x)**2
	else:
		partial_result = 0
		if string2.x < string1.x:
			partial_result += (string2.x - string1.x)**2
		else:
			partial_result += (string1.x + string1.w - string2.x)**2
		if string2.y < string1.y:
			partial_result += (string2.y - string1.y)**2
		else:
			partial_result += (string1.y + string1.h - string2.y)**2
		return partial_result


def eval_string(string1: str, string2: str):
	for i in range(0, min(round(len(string1)/2), MAX_STRIP)):
		if string2.find(lstripi(string1, i)) != -1 or string2.find(rstripi(string1, i)) != -1:
			return (len(string1) - i) * 100 / (len(string1) + len(string2) - i)
	return 0


def full_eval(string1, string2):
	return (_eval_position(string1, string2) * POS_WEIGHT + POS_BIAS) / (eval_string(string2.string, string1.string) * STR_WEIGHT + STR_BIAS)


def full_search(string: StringOut):
	ret_list = []
	for s in positioned_strings:
		try:
			evaluation = full_eval(string, s)
			if evaluation > MIN_EVAL:
				ret_list.append((evaluation, s.string))
		except ZeroDivisionError:
			pass
	ret_list.sort()
	try:
		return ret_list[0]
	except IndexError:
		pass


def separate_words(dictionary):
	working_text = dictionary["texto"]

	ret_list = []
	line_list = []
	word_list = ""
	for i in range(len(working_text)):
		c = working_text.__getitem__(i)
		if c == '\n':
			line_list.append(word_list)
			ret_list.append(line_list)
			word_list = ""
			line_list = []
		elif c == " ":
			line_list.append(word_list)
			word_list = ""
		else:
			word_list += c
	line_list.append(word_list)
	ret_list.append(line_list)
	return ret_list


def connect(texted_area):
	area = texted_area["bounding_box"].copy()
	area["x"] += ERROR_MARGIN
	area["y"] += ERROR_MARGIN
	area["width"] += ERROR_MARGIN * 2
	area["height"] += ERROR_MARGIN * 2

	word_list = []
	for line in separate_words(texted_area):
		line_list = []
		for s in line:
			line_list.append(StringOut(s, area["x"], area["y"], area["width"], area["height"]))
		word_list.append(line_list)

	print(word_list[0][0].string)
	try:
		print(full_search(word_list[0][0])[0])
	except:
		print("no match")


tree = Tree.parse("xml.xml")
positioned_strings = loop_through(tree.getroot())
with open("json.json", "rb") as jn:
	json_dict = json.load(jn)
	json_texts = from_json_dict(json_dict)
for text in json_texts:
	connect(text)
