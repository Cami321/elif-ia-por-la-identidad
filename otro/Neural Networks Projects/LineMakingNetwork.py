import tensorflow as tf
from tensorflow import keras as k

inputs = k.Input(shape=(50,))
x1 = k.layers.Dense(40, activation=tf.nn.relu)(inputs)
x2 = k.layers.Dense(30, activation=tf.nn.relu)(x1)
x3 = k.layers.Dense(20, activation=tf.nn.relu)(x2)
x4 = k.layers.Dense(10, activation=tf.nn.relu)(x3)
x5 = k.layers.Dense(5, activation=tf.nn.relu)(x4)
output = k.layers.Dense(1, activation=tf.nn.relu)(x5)
model = k.Model(inputs, output)

