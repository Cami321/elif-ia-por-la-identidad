import json
import XMLTreatment as Xml
import xml.etree.ElementTree as Tree
from tkinter import *


def _search_line(master, string: Xml.StringOut):
    if master.tag.lstrip("{http://www.loc.gov/standards/alto/ns-v3#}") == "TextLine":
        for entry in master:
            if Xml.from_dict(entry.attrib) == string:
                ret_str = ""
                for s in master:
                    ret_str += s["CONTENT"] + " "
                return ret_str
    else:
        for entry in master:
            recursive = _search_line(entry, string)
            if recursive is not None:
                return recursive


def get_current_line(string: Xml.StringOut):
    master = Tree.parse("xml.xml").getroot()
    return _search_line(master, string)


def create_match(xml: Xml.StringOut, json_ij, json_word):
    body = {
        "org_word": json_word,
        "word": xml.string,
        "x": xml.x,
        "y": xml.y,
        "w": xml.w,
        "h": xml.h}

    key = f"l{json_ij[0]}w{json_ij[1]}"

    return key, body


class MainWindow:
    def __init__(self, _root, _texts):
        self.root = _root
        self.texts = _texts
        self.pos = None
        self.words = None
        self.matches = None
        self.memory = []
        self.text_memory = []
        self.match_i = 0
        self.w_ij = (0, 0)

        text_pos_frame = Frame(self.root)
        self.text_cord = {
            "x1": Label(text_pos_frame),
            "y1": Label(text_pos_frame),
            "x2": Label(text_pos_frame),
            "y2": Label(text_pos_frame)}
        Label(text_pos_frame, text="from:").grid(columnspan=2, column=1, row=1)
        Label(text_pos_frame, text="to:").grid(columnspan=2, column=1, row=3)
        self.text_cord["x1"].grid(column=1, row=2)
        self.text_cord["y1"].grid(column=2, row=2)
        self.text_cord["x2"].grid(column=1, row=4)
        self.text_cord["y2"].grid(column=2, row=4)
        text_pos_frame.pack(side=TOP)

        text_frame = Frame(self.root)
        self.done = Label(text_frame)
        self.doing = Label(text_frame)
        self.to_do = Label(text_frame)
        self.done.pack(side=TOP)
        self.doing.pack()
        self.to_do.pack(side=BOTTOM)
        text_frame.pack()

        match_frame = Frame(self.root)
        self.match = Label(match_frame)
        self.match_line = Label(match_frame)
        self.next_match = Button(match_frame, text="∨", command=self.change_match)
        self.previous_match = Button(match_frame, text="∧", command=lambda: self.change_match(previous=True))
        self.match_button = Button(self.root, text="Match", command=self.match_selected)
        self.match_cord = {
            "x1": Label(match_frame),
            "y1": Label(match_frame),
            "x2": Label(match_frame),
            "y2": Label(match_frame)}

        self.match.grid(column=1, columnspan=3, row=1)
        self.match_line.grid(column=1, columnspan=3, row=2)
        self.next_match.grid(column=4, row=2)
        self.previous_match.grid(column=4, row=1)
        self.match_cord["x1"].grid(column=1, row=3)
        self.match_cord["y1"].grid(column=2, row=3)
        self.match_cord["x2"].grid(column=3, row=3)
        self.match_cord["y2"].grid(column=4, row=3)
        match_frame.pack(side=BOTTOM)

        self.next_text()

    def next_text(self):
        text = self.texts[0]
        self.memory.append((f"x{self.pos['x']}y{self.pos['y']}", dict(self.text_memory)))
        self.texts.remove(text)
        self.pos = text["bounding_box"]
        self.words = Xml.separate_words(text["texto"])

        self.text_cord["x1"]["text"] = f"X: {self.pos['x']}"
        self.text_cord["y1"]["text"] = f"Y: {self.pos['y']}"
        self.text_cord["x2"]["text"] = f"X: {self.pos['x'] + self.pos['width']}"
        self.text_cord["y2"]["text"] = f"Y: {self.pos['x'] + self.pos['height']}"

        self.doing["text"] = self.words[0][0]
        for isnt_first, word in enumerate(self.words[0]):
            if not isnt_first:
                pass
            self.to_do["text"] += word + " "

        graded_matches = [(Xml.eval_string(self.words[0][0], s.string), s) for s in posible_matches]
        graded_matches.sort(reverse=True)
        self.matches = [string for _, string in graded_matches]
        self.match["text"] = self.matches[0].string
        self.match_line["text"] = get_current_line(self.matches[0])
        self.match_cord["x1"]["text"] = f"X: {self.matches[0].x}"
        self.match_cord["x1"]["text"] = f"Y: {self.matches[0].y}"
        self.match_cord["x1"]["text"] = f"X: {self.matches[0].x + self.matches[0].w}"
        self.match_cord["x1"]["text"] = f"Y: {self.matches[0].y + self.matches[0].h}"

    def change_match(self, previous=False):
        org_i = self.match_i
        if previous:
            self.match_i -= 1
        else:
            self.match_i += 1

        if 0 < self.match_i < len(self.matches):
            self.match["text"] = self.matches[self.match_i].string
            self.match_line["text"] = get_current_line(self.matches[self.match_i])
            self.match_cord["x1"]["text"] = f"X: {self.matches[self.match_i].x}"
            self.match_cord["x1"]["text"] = f"Y: {self.matches[self.match_i].y}"
            self.match_cord["x1"]["text"] = f"X: {self.matches[self.match_i].x + self.matches[self.match_i].w}"
            self.match_cord["x1"]["text"] = f"Y: {self.matches[self.match_i].y + self.matches[self.match_i].h}"
        else:
            self.match_i = org_i

    def match_selected(self):
        result = create_match(self.matches[self.match_i], self.w_ij, self.words[self.w_ij[0]][self.w_ij[1]])
        self.text_memory.append(result)
        posible_matches.remove(self.matches[self.match_i])
        self.next_word()

    def next_word(self):
        if self.w_ij[1] + 1 < len(self.words[self.w_ij[0]]):
            self.w_ij[1] += 1

        else:
            self.w_ij = (self.w_ij[0], 0)
            if self.w_ij[0] + 1 < len(self.words):
                self.w_ij = (self.w_ij[0] + 1, 0)
            else:
                self.w_ij = (0, 0)
                self.next_text()
                return

        self.doing["text"] = self.words[self.w_ij[0]][self.w_ij[1]]
        self.done["text"] = ""
        self.to_do["text"] = ""
        for i, word in enumerate(self.words[self.w_ij[0]]):
            if i < self.w_ij[1]:
                self.done["text"] += word + " "
            elif i > self.w_ij[1]:
                self.to_do["text"] += word + " "

        self.match_i = 0
        graded_matches = [(Xml.eval_string(self.words[0][0], s.string), s) for s in posible_matches]
        graded_matches.sort(reverse=True)
        self.matches = [string for _, string in graded_matches]
        self.match["text"] = self.matches[0].string
        self.match_line["text"] = get_current_line(self.matches[0])
        self.match_cord["x1"]["text"] = f"X: {self.matches[0].x}"
        self.match_cord["x1"]["text"] = f"Y: {self.matches[0].y}"
        self.match_cord["x1"]["text"] = f"X: {self.matches[0].x + self.matches[0].w}"
        self.match_cord["x1"]["text"] = f"Y: {self.matches[0].y + self.matches[0].h}"


tree = Tree.parse("xml.xml")
posible_matches = Xml.loop_through(tree.getroot())

with open("json.json", "rb") as jn:
    json_dict = json.load(jn)
    json_texts = Xml.from_json_dict(json_dict)

for __text in json_texts:
    root = Tk()
    window = MainWindow(root, __text)
    root.mainloop()
