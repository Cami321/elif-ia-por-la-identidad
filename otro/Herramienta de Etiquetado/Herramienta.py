from tkinter import *
from json_reader import main as rmain
from json_writter import main as wmain


class TextHolder():
	def __init__(self, text):
		self.text = text
	
	def get(self):
		return self.text
	
	def set(self, newtext):
		self.text = newtext

class MainWindow():
	def __init__(self, root, data):
		self.path, self.dict = data
		self.path = self.path.split("-", 1)[1].split(".jpg")[0]
		self.root = root
		self.root.title(self.path)
		self.texts, self.variableLabels, self.assignedNote = parseDict(self.dict)

		self.frame = Frame(root)
		self.showHeader()
		self.showParts()
		Button(self.frame, text="Finish", command=self.closeAndConvert).grid(column=2, row=3)
		self.frame.grid()


	def showHeader(self):
		self.headerFrame = Frame(self.frame)

		Label(self.headerFrame, text="Diario", font="TkHeadingFont").grid(column=1,columnspan=2 ,row=1)
		Label(self.headerFrame, text="Fecha", font="TkHeadingFont").grid(column=3, columnspan=2,row=1)

		diario_cord = ()
		if self.dict["Diario"]:
			diario_cord = (self.dict['Diario'][0]['x'], self.dict['Diario'][0]['y'])
			Label(self.headerFrame, text=f"x: {round(diario_cord[0])}").grid(column=1, row=3)
			Label(self.headerFrame, text=f"y: {round(diario_cord[1])}").grid(column=2, row=3)
			
		fecha_cord = ()
		if self.dict["Fecha"]:
			fecha_cord = (self.dict['Fecha'][0]['x'], self.dict['Fecha'][0]['y'])
			Label(self.headerFrame, text=f"x: {round(fecha_cord[0])}").grid(column=3, row=3)
			Label(self.headerFrame, text=f"y: {round(fecha_cord[1])}").grid(column=4, row=3)

		textbox1 = Text(self.headerFrame, height=1, width=20)
		textbox1.grid(column=1, columnspan=2, row=2)
		textbox2 = Text(self.headerFrame, height=1, width=20)
		textbox2.grid(column=3, columnspan=2,row=2)

		self.headerFrame.grid(column=1, columnspan=2, row=1)
		self.logicHeader = {"Diario": (textbox1, diario_cord), "Fecha": (textbox2, fecha_cord)}
	
	def showParts(self):
		self.scrollCanvas = Canvas(self.frame)
		self.partFrame = Frame(self.scrollCanvas, width=1000, height=1000)
		scrollbar = Scrollbar(self.frame, orient=VERTICAL, command=self.scrollCanvas.yview)
		for type in LABEL_LIST_TYPES:
			for i, label in enumerate(self.dict[type]):
				self._showPart(label, type, i)
		
		self.scrollCanvas["width"] = self.partFrame.winfo_reqwidth()-400
		self.scrollCanvas["height"] = 900
		coordinates= (0, 0, self.partFrame.winfo_reqwidth(),5000)
		
		self.scrollCanvas.grid(column=1, row=2, sticky=NW)
		scrollbar.grid(column=2, row=2)
		self.scrollCanvas.create_window(0, 0, window=self.partFrame, anchor=NW)
		self.scrollCanvas.configure(yscrollcommand=scrollbar.set, scrollregion=coordinates)

	def _showPart(self, label, type, i):
		frame = Frame(self.partFrame)
		Label(frame, text=type, font="TkHeadingFont").grid(column=1, columnspan=5, row=1)
		self.variableLabels[type][i] = Label(frame, text=self.texts[type][i].get(), width=50, height=3, wraplength=400, anchor=NW, font="TkTooltipFont")
		self.variableLabels[type][i].grid(column=1, columnspan=4, row=2, rowspan=2)
		Button(frame, text="Write", command=lambda: self._openWin(type, i)).grid(column=5, row=2, rowspan=2)
		Label(frame, text=f"x: {round(label['x'])}").grid(column=1,row=4)
		Label(frame, text=f"y: {round(label['y'])}").grid(column=2,row=4)
		Label(frame, text=f"width: {round(label['width'])}").grid(column=3,row=4)
		Label(frame, text=f"height: {round(label['height'])}").grid(column=4,row=4)
		if type != "Página":
			self.assignedNote[type][i] = Spinbox(frame, from_=1, to=8, width=5)
			self.assignedNote[type][i].grid(column=0, row=2, rowspan=2)
		else:
			Label(frame, text="", width=5).grid(column=0, row=2, rowspan=2)
		frame.pack(side=TOP)

	def _openWin(self, type, i):
		openWindow((lambda root: WrittingWindow(self.texts[type][i], self.variableLabels[type][i], root)), self.root)

	def closeAndConvert(self):
		data = self.collectData()
		print(data)
		wmain(self.path, data)
		self.root.destroy()

	def collectData(self):
		d_bounding = {}
		if self.dict["Diario"]:
			d_bounding = {"x":self.dict['Diario'][0]['x'], "y":self.dict['Diario'][0]['y'], "width":self.dict['Diario'][0]['width'], "height":self.dict['Diario'][0]['height']}
		d_text = self.logicHeader["Diario"][0].get("1.0", "end - 1 chars")
		diario = {"texto": d_text, "bounding_box": d_bounding}

		f_bounding = {}
		if self.dict["Fecha"]:
			f_bounding = {"x":self.dict['Fecha'][0]['x'], "y":self.dict['Fecha'][0]['y'], "width":self.dict['Fecha'][0]['width'], "height":self.dict['Fecha'][0]['height']}
		f_text = self.logicHeader["Fecha"][0].get("1.0", "end - 1 chars")
		fecha = {"texto": f_text, "bounding_box": f_bounding}

		paginas = []
		for i, entry in enumerate(self.dict["Página"]):
			p_text = self.texts["Página"][i].get()
			p_bounding = {"x": entry['x'], "y": entry['y'], "width": entry['width'], "height": entry['height']}
			paginas.append({"texto": p_text, "bounding_box": p_bounding})

		notes_amount = 1
		for type in LABEL_NOTES_TYPES:
			for spinbox in self.assignedNote[type]:
				if int(spinbox.get()) > notes_amount:
					notes_amount = int(spinbox.get())
		
		notes = [{"Volanta": [], "Título": [], "Copete": [], "Destacado": [], "Cuerpo": [], "Fotografía": [], "Epígrafe": [], "Firma": []} for _ in range(notes_amount)]
		for type in LABEL_NOTES_TYPES:
			for i, entry in enumerate(self.dict[type]):
				l_text = self.texts[type][i].get()
				l_bounding = {"x":entry['x'], "y":entry['y'], "width":entry['width'], "height":entry['height']}
				notes[int(self.assignedNote[type][i].get())-1][type].append({"texto": l_text, "bounding_box": l_bounding})

		return {"Diario":diario, "Fecha":fecha, "Notas":notes, "Página":paginas}





class WrittingWindow():
	def __init__(self, text, pre_show, root):
		self.label = pre_show
		self.text = text
		self.root = root
		self.frame = Frame(self.root)

		self.saveBtn = Button(self.frame, text="Quit and Save", command=lambda :self.saveAndQuit())
		self.saveBtn.grid(column=6, row=2)

		self.textbox = Text(self.frame, height=30, width=100)
		self.textbox.insert("1.0", text.get())
		self.textbox.grid(column=1, columnspan=6, row=1)

		self.scrollbar = Scrollbar(self.frame, orient=VERTICAL, command=self.textbox.yview)
		self.scrollbar.grid(column=7, row=1, rowspan=2)
		self.textbox.configure(yscrollcommand=self.scrollbar.set)

		self.frame.pack()

	def saveAndQuit(self):
		self.text.set(self.textbox.get("1.0", "end - 1 chars"))
		self.label.config(text=self.text.get())
		self.root.destroy()
		

LABEL_TYPES = ["Diario", "Fecha", "Volanta", "Título", "Copete", "Cuerpo", "Fotografía", "Epígrafe", "Destacado", "Firma", "Página"]
LABEL_LIST_TYPES = ["Volanta", "Título", "Copete", "Cuerpo", "Fotografía", "Epígrafe", "Destacado", "Firma", "Página"]
LABEL_NOTES_TYPES = ["Volanta", "Título", "Copete", "Cuerpo", "Fotografía", "Epígrafe", "Destacado", "Firma"]

def openWindow(_constructor, root):
	window_root = Toplevel(root)
	_constructor(window_root)

def parseDict(dic):
	pre_res1 = []
	pre_res2 = []
	pre_res3 = []
	for type in LABEL_TYPES:
		pre_res1.append((type, [TextHolder("") for _ in range(dic[type].__len__())]))
		if type != "Diario" and type != "Fecha":
			pre_res2.append((type, [Label() for _ in range(dic[type].__len__())]))
			if type != "Página":
				pre_res3.append((type, [Spinbox() for _ in range(dic[type].__len__())]))
	return (dict(pre_res1), dict(pre_res2), dict(pre_res3))

for n in (9,38,39):
	_data = rmain(n)
	img = _data[0]
	dic = _data[1]
	root = Tk()
	root.wm_geometry(f"{960}x{1080}+{0}+{0}")
	window = MainWindow(root, _data)
	root.mainloop()
