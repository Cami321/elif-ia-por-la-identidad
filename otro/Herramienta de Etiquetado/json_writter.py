import json

def main(path, _data):
    json_str = json.dumps(_data, sort_keys=False, indent=3, ensure_ascii=False)

    with open(f"jsons/{path}.json", "w+") as file:
        file.write(json_str)

