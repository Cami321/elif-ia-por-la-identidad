import pandas as pd


def percentageToPixels(perc, base):
    return perc/100.0 * base

def filterData(label, data):
    return pd.DataFrame([entry for entry in data.iloc if entry["rectanglelabels"] == [label]])

def getLabel(_data, label):
    data = filterData(label, _data)
    return [getLabelInfo(dt) for dt in data.iloc]

def getLabelInfo(data):
    return {"x" : percentageToPixels(data["x"], data["original_width"]),
            "y": percentageToPixels(data["y"], data["original_height"]),
            "width": percentageToPixels(data["width"], data["original_width"]),
            "height": percentageToPixels(data["height"], data["original_height"])}


def main(num):
    data = pd.read_json("labels.json").iloc[num]

    labels = pd.DataFrame(data["label"])
    get = lambda label: getLabel(labels, label)

    dictionarie = {
        "Diario": get("Diario"),
        "Fecha": get("Fecha"),
        "Volanta": get("Volanta"),
        "Título": get("Título"),
        "Copete": get("Copete"),
        "Cuerpo": get("Cuerpo"),
        "Fotografía": get("Fotografía"),
        "Epígrafe": get("Epígrafe"),
        "Destacado": get("Destacado"),
        "Firma": get("Firma"),
        "Página": get("Página")}
    
    return (data["image"], dictionarie)
